//@author: vvvv group
//@help: This is a very basic template. Use it to start writing your own effects. If you want effects with lighting start from one of the GouraudXXXX or PhongXXXX effects
//@tags: hlsl
//@credits:

// --------------------------------------------------------------------------------------------------
// PARAMETERS:
// --------------------------------------------------------------------------------------------------

//transforms
float4x4 tW: WORLD;        //the models world matrix
float4x4 tV: VIEW;         //view matrix as set via Renderer (EX9)
float4x4 tP: PROJECTION;
float4x4 tWV: WORLDVIEW;
float4x4 tWVP: WORLDVIEWPROJECTION;

float3 LightPosition = {10.0f,10.0f,0.0f};

float4 LightColor : COLOR = {1.0f,1.0f,1.0f,1.0f};

float4 ObjectColor : COLOR = {1.0f, 1.0f, 1.0f, 1.0f};

float4 AmbientColor : COLOR = {0.25f, 0.25f, 0.25f, 1.0f};

float4 SpecularColor : COLOR = {0.2f, 0.2f, 0.2f, 1.0f};

float Shininess = 10;

float Alpha = 1;

float Amplitude = 1;

texture Tex <string UIName="Texture";>;
sampler Samp = sampler_state
{
	Texture = (Tex);
    MipFilter = LINEAR;
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};


///// Structs /////
struct app2vs
{
	float4 Pos			: POSITION;
	float3 Normal		: NORMAL;
	float4 texcoord		: TEXCOORD0;
};

struct vs2ps
{
    float4 Pos  		: SV_POSITION;
	float4 PosWorld		: TEXCOORD0;
	float4 NormalDir	: TEXCOORD1;
	float4 tex			: TEXCOORD2;
};

// --------------------------------------------------------------------------------------------------
// VERTEXSHADERS
// --------------------------------------------------------------------------------------------------
vs2ps VS(app2vs In)
{
    //declare output struct
    vs2ps Out;
	
	Out.PosWorld = mul(In.Pos, tW);
	Out.NormalDir = normalize( mul(In.Normal, tWV));
	
	float4 texValue = tex2Dlod(Samp, In.texcoord);
	Out.Pos = In.Pos;
	Out.Pos.xyz *= 1 + (1 - texValue.rgb) * Amplitude;
	
	Out.Pos = mul(Out.Pos, tWVP);
	
	Out.tex = In.texcoord;

    return Out;
}

// --------------------------------------------------------------------------------------------------
// PIXELSHADERS:
// --------------------------------------------------------------------------------------------------

float4 PS(vs2ps In): COLOR
{
	float3 viewDirection = normalize( -mul( In.PosWorld, tV));
	float3 lightDirection = normalize( mul(LightPosition - In.PosWorld, tV));
	float atten = 1.0;
	
	float3 diffuseReflection = atten * LightColor.rgb * saturate( dot( In.NormalDir, lightDirection));
	
	float3 specularReflection = atten * SpecularColor.rgb * saturate( dot( In.NormalDir, lightDirection));
	float3 specularPower = pow( saturate( dot( reflect( -lightDirection, In.NormalDir), viewDirection)), Shininess);
	specularReflection *= specularPower;
	
	float3 lightFinal = AmbientColor + diffuseReflection + specularReflection;
	
	float4 texColor = tex2D(Samp, In.tex);
	
	return float4(lightFinal * ObjectColor.rgb, Alpha);
}

// --------------------------------------------------------------------------------------------------
// TECHNIQUES:
// --------------------------------------------------------------------------------------------------

technique TSimpleShader
{
    pass P0
    {
        //Wrap0 = U;  // useful when mesh is round like a sphere
        VertexShader = compile vs_3_0 VS();
        PixelShader  = compile ps_2_0 PS();
    }
}
