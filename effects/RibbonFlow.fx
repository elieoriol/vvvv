//@author: vvvv group
//@help: This is a very basic template. Use it to start writing your own effects. If you want effects with lighting start from one of the GouraudXXXX or PhongXXXX effects
//@tags: hlsl
//@credits:

// --------------------------------------------------------------------------------------------------
// PARAMETERS:
// --------------------------------------------------------------------------------------------------

//transforms
float4x4 tW: WORLD;        //the models world matrix
float4x4 tV: VIEW;         //view matrix as set via Renderer (EX9)
float4x4 tP: PROJECTION;
float4x4 tWVP: WORLDVIEWPROJECTION;

//color
float4 Color : COLOR = {1, 1, 1, 1};

//texture
texture Tex <string uiname="Texture";>;
sampler Samp = sampler_state    //sampler for doing the texture-lookup
{
    Texture   = (Tex);          //apply a texture to the sampler
    MipFilter = LINEAR;         //sampler states
    MinFilter = LINEAR;
    MagFilter = LINEAR;
};

//texture transformation marked with semantic TEXTUREMATRIX to achieve symmetric transformations
float4x4 tTex: TEXTUREMATRIX <string uiname="Texture Transform";>;

float FrontCoeff <float uimin=0; float uimax=1;> = 1;
float VanishLength <float uimin=0; float uimax=2;> = 0.1;
float Alpha <float uimin=0; float uimax=1;> = 1;

//the data structure: "vertexshader to pixelshader"
//used as output data with the VS function
//and as input data with the PS function
struct vsInput
{
	float4 PosO  	: POSITION;
    float4 TexCd 	: TEXCOORD0;
};

struct psInput
{
    float4 Pos  	: SV_Position;
    float2 TexCd 	: TEXCOORD0;
	float4 Color	: COLOR;
};

// --------------------------------------------------------------------------------------------------
// VERTEXSHADERS
// --------------------------------------------------------------------------------------------------
psInput VS(vsInput In)
{
    //declare output struct
    psInput Out;

    //transform position
    Out.Pos = mul(In.PosO, tWVP);
	
	float4 col = float4(1, 1, 1, 1);
	float headPos = 2 * FrontCoeff - 1;
	float xPos = In.PosO.x;
	if (xPos > headPos) {
		col.rgb = 0;
	}
	else if (xPos > headPos - VanishLength) {
		col.rgb = lerp(0, 1, (headPos-xPos)/VanishLength);
	}
	
	Out.Color = col;
	
	//transform texturecoordinates
    Out.TexCd = mul(In.TexCd, tTex);

    return Out;
}

// --------------------------------------------------------------------------------------------------
// PIXELSHADERS:
// --------------------------------------------------------------------------------------------------

float4 PS(psInput In): COLOR
{
    float4 col = tex2D(Samp, In.TexCd);

	col *= In.Color;
	col *= Color;
	col.a *= Alpha;
	return col;
}

// --------------------------------------------------------------------------------------------------
// TECHNIQUES:
// --------------------------------------------------------------------------------------------------

technique TSimpleShader
{
    pass P0
    {
        //Wrap0 = U;  // useful when mesh is round like a sphere
        VertexShader = compile vs_1_1 VS();
        PixelShader  = compile ps_2_0 PS();
    }
}

technique TFixedFunction
{
    pass P0
    {
        //transforms
        WorldTransform[0]   = (tW);
        ViewTransform       = (tV);
        ProjectionTransform = (tP);

        //texturing
        Sampler[0] = (Samp);
        TextureTransform[0] = (tTex);
        TexCoordIndex[0] = 0;
        TextureTransformFlags[0] = COUNT2;
        //Wrap0 = U;  // useful when mesh is round like a sphere
        
        Lighting       = FALSE;

        //shaders
        VertexShader = NULL;
        PixelShader  = NULL;
    }
}
