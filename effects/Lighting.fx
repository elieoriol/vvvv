//@author: vvvv group
//@help: This is a very basic template. Use it to start writing your own effects. If you want effects with lighting start from one of the GouraudXXXX or PhongXXXX effects
//@tags: hlsl
//@credits:

// --------------------------------------------------------------------------------------------------
// PARAMETERS:
// --------------------------------------------------------------------------------------------------

//transforms
float4x4 tW: WORLD;        //the models world matrix
float4x4 tV: VIEW;         //view matrix as set via Renderer (EX9)
float4x4 tP: PROJECTION;
float4x4 tWV: WORLDVIEW;
float4x4 tWVP: WORLDVIEWPROJECTION;

float3 LightPosition
<
	string UIName = "Light Position";
> = {10.0f,10.0f,0.0f};

float4 LightColor : COLOR
<
	string UIName = "Light Color";
> = {1.0f,1.0f,1.0f,1.0f};

float4 ObjectColor : COLOR
<
	String UIName = "Object Color";
> = {1.0f, 1.0f, 1.0f, 1.0f};

float4 AmbientColor : COLOR
<
	String UIName = "Ambient Color";
> = {0.25f, 0.25f, 0.25f, 1.0f};

float4 SpecularColor : COLOR
<
	String UIName = "Specular Color";
> = {0.2f, 0.2f, 0.2f, 1.0f};

float Shininess
<
	String UIName = "Shininess";
> = 10;

float4 RimColor : COLOR
<
	String UIName = "Rim Color";
> = {0.1f, 0.1f, 0.5f, 1.0f};

float RimPower
<
	String UIName = "Rim Power";
> = 10;

float Alpha
<
	String UIName = "Alpha";
> = 0.5;

float Amplitude
<
	String UIName = "Amplitude";
> = 0.5;

float FrequencyX
<
	String UIName = "FrequencyX";
> = 10;

float FrequencyY
<
	String UIName = "FrequencyY";
> = 10;


///// Structs /////
struct app2vs
{
	float4 Pos			: POSITION;
	float3 Normal		: NORMAL;
};

struct vs2ps
{
    float4 Pos  		: SV_POSITION;
	float4 PosWorld		: TEXCOORD0;
	float4 NormalDir	: TEXCOORD1;
};

// --------------------------------------------------------------------------------------------------
// VERTEXSHADERS
// --------------------------------------------------------------------------------------------------
vs2ps VS(app2vs In)
{
    //declare output struct
    vs2ps Out;
	
	Out.PosWorld = mul(In.Pos, tW);
	Out.NormalDir = normalize( mul(In.Normal, tWV));
	
	Out.Pos = mul(In.Pos, tWVP);
	Out.Pos.x += sin(Out.Pos.z * FrequencyX) * Amplitude;
	Out.Pos.y += sin(Out.Pos.z * FrequencyY) * Amplitude;

    return Out;
}

// --------------------------------------------------------------------------------------------------
// PIXELSHADERS:
// --------------------------------------------------------------------------------------------------

float4 PS(vs2ps In): COLOR
{
	float3 viewDirection = normalize( -mul( In.PosWorld, tV));
	float3 lightDirection = normalize( mul(LightPosition - In.PosWorld, tV));
	float atten = 1.0;
	
	float3 diffuseReflection = atten * LightColor.rgb * saturate( dot( In.NormalDir, lightDirection));
	
	float3 specularReflection = atten * SpecularColor.rgb * saturate( dot( In.NormalDir, lightDirection));
	float3 specularPower = pow( saturate( dot( reflect( -lightDirection, In.NormalDir), viewDirection)), Shininess);
	specularReflection *= specularPower;
	
	float rim =	1 - saturate(dot( normalize(viewDirection), In.NormalDir));
	float3 rimLighting = atten * LightColor.rgb * RimColor * saturate( dot( In.NormalDir, lightDirection)) * pow(rim, RimPower);
	
	float3 lightFinal = AmbientColor + diffuseReflection + specularReflection + rimLighting;
	
	return float4(lightFinal * ObjectColor.rgb, Alpha);
}

// --------------------------------------------------------------------------------------------------
// TECHNIQUES:
// --------------------------------------------------------------------------------------------------

technique TSimpleShader
{
    pass P0
    {
        //Wrap0 = U;  // useful when mesh is round like a sphere
        VertexShader = compile vs_1_1 VS();
        PixelShader  = compile ps_2_0 PS();
    }
}
