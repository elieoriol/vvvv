
struct vsInputTextured
{
    float4 posObject : POSITION;
	float4 uv: TEXCOORD0;
	uint iv : SV_VertexID;
	uint ii : SV_InstanceID; 
};

struct psInputTextured
{
    float4 posScreen : SV_Position;
    float4 uv: TEXCOORD0;
	float4 uvInf:TEXCOORD1;
};

Texture2D inputTexture <string uiname="Texture";>;



struct BufferIn
{
	float3 pos : POSITION;
	float3 pos2;
	float3 uv;

};
StructuredBuffer<BufferIn> ControlPointBuffer;


SamplerState linearSampler <string uiname="Sampler State";>
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};

cbuffer cbPerDraw : register(b0)
{
	float4x4 tVP : VIEWPROJECTION;
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float Alpha <float uimin=0.0; float uimax=1.0;> = 1; 
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
	float4x4 tColor <string uiname="Color Transform";>;
};

cbuffer cbTextureData : register(b2)
{
	float4x4 tTex <string uiname="Texture Transform"; bool uvspace=true; >;
};

float2 size=float2(0.01,0.06);

#define PI 3.1415926535897932384626433832795
psInputTextured VS_Textured(vsInputTextured input)
{
	psInputTextured output;
	
	float4 posL = input.posObject;
	
	float2 dir=ControlPointBuffer[input.ii].pos2.xy;
	output.uvInf=0;
	output.uvInf.xy=dir;
	output.uvInf.z=abs(posL.z);

	float2 velL =dir;

	if (length(dir)==0) dir=float2(0.00001,0);
	
	
	float angle= atan2(dir.y,dir.x);
	angle+=PI*.5;
	
	//angle = -PI/2;
	float s = sin(angle);
	float c = cos(angle);
	
	posL.xy*=size;
	posL.y-=size.y/2.;
	posL.y*=length(dir.xy)*40;
	
	float2x2 rotMat = {c,-s,
				       s, c};
	float2 newXY = mul(rotMat,posL.xy);
	posL.x = newXY.x;
    posL.y = newXY.y;
    posL.xy+=ControlPointBuffer[input.ii].pos.xy *float2(1.7778,1) ;
	output.posScreen = mul(posL,tVP);
	output.uv = mul(input.uv, tTex);
	return output;
}


float4 PS_Textured(psInputTextured input): SV_Target
{   

	float4 col = abs(float4(input.uvInf.xy,0,1))*200 * input.uv.y;
    return col;
}

technique11 Constant <string noTexCdFallback="ConstantNoTexture"; >
{
	pass P0
	{
		SetVertexShader( CompileShader( vs_4_0, VS_Textured() ) );
		SetPixelShader( CompileShader( ps_4_0, PS_Textured() ) );
	}
}







