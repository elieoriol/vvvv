//@author: vux
//@help: template for standard shaders
//@tags: template
//@credits: 

Texture2D texture2d <string uiname="Texture";>;

SamplerState linearSampler : IMMUTABLE
{
    Filter = MIN_MAG_MIP_LINEAR;
    AddressU = Clamp;
    AddressV = Clamp;
};
 
float4x4 tTex: TEXTUREMATRIX <string uiname="Texture Transform";>;

float FrontCoeff <float uimin=0; float uimax=1;> = 1;
float VanishLength <float uimin=0; float uimax=2;> = 0.1;

cbuffer cbPerDraw : register( b0 )
{
	float4x4 tV : VIEW;	
	float4x4 tVP : VIEWPROJECTION;	
};

cbuffer cbPerObj : register( b1 )
{
	float4x4 tW : WORLD;
	float4 cAmb <bool color=true;String uiname="Color";> = { 1.0f,1.0f,1.0f,1.0f };
};

struct vsInput
{
	float4 PosO  	: POSITION;
    float4 TexCd 	: TEXCOORD0;
};

struct psInput
{
    float4 Pos  	: SV_Position;
    float2 TexCd 	: TEXCOORD0;
};

psInput VS(vsInput In)
{
    //declare output struct
    psInput Out;

    //transform position
    Out.Pos = mul(In.PosO, mul(tW, tVP));
	
	//transform texturecoordinates
    Out.TexCd = mul(In.TexCd, tTex);

    return Out;
}

float4 PS(psInput In, uint pID : SV_PrimitiveID): SV_Target
{
	uint frontID = uint(FrontCoeff*400);
	uint VanishID = uint(VanishLength*400);
	
	int diff = pID - frontID;
	
	float4 col = texture2d.Sample(linearSampler,In.TexCd.xy) * cAmb;
	if (diff >= 0) {
		col.rgb = 0;
	}
	else if (diff + VanishID >= 0) {
		col.rgb = lerp(0, 1, -float(diff)/float(VanishID));
	}

    return col;
}





technique10 Constant
{
	pass P0
    {
        //Wrap0 = U;  // useful when mesh is round like a sphere
        SetVertexShader( CompileShader( vs_5_0, VS()));
        SetPixelShader( CompileShader( ps_5_0, PS()));
    }
}
